<picture>
  <source media="(max-width: 1440px) and (prefers-color-scheme: light)" srcset="assets/images/750.svg">
  <source media="(max-width: 660px) and (prefers-color-scheme: light)" srcset="assets/images/600.svg">
  <source media="(max-width: 430px) and (prefers-color-scheme: light)" srcset="assets/images/350.svg">
  <source media="(max-width: 1440px) and (prefers-color-scheme: dark)" srcset="assets/images/750-dark.svg">
  <source media="(max-width: 660px) and (prefers-color-scheme: dark)" srcset="assets/images/600-dark.svg">
  <source media="(max-width: 430px) and (prefers-color-scheme: dark)" srcset="assets/images/350-dark.svg">
  <img 
    alt="Table containing the language flag icons, the translations and the logotype"
    title="Complete dotfiles at the Piltovan style"
    src="assets/images/750.svg">
</picture>