#!/usr/bin/env sh

##================##
##
## 🇬🇧 Functions
## 🇫🇷 Fonctions
## 🇪🇸 Funciones
## 🇮🇹 Funzioni
## 🇵🇹 Funções
## 🇩🇪 Funktionen
## 🇳🇱 Functies
## 🏴󠁥󠁳󠁣󠁴󠁿 Funcions
## 🇷🇴 Funcții
## 🇬🇷 Λειτουργίες
## 🇬🇪 ფუნქციები
## 🇺🇦 Функції
##
##================##

# 🇬🇧 GitHub and GitLab accounts switches
# 🇫🇷 Commutateurs de comptes de GitHub et GitLab
# 🇪🇸 Cambios de cuentas de GitHub y GitLab
# 🇮🇹 Scambi di account di GitHub e GitLab
# 🇵🇹 Trocas de contas do GitHub e GitLab
# 🇩🇪 Wechsel von Konten von GitHub und GitLab
# 🇳🇱 Wissels van accounts van GitHub en GitLab
# 🏴󠁥󠁳󠁣󠁴󠁿 Canvis de comptes de GitHub i GitLab
# 🇷🇴 Schimbări de conturi de GitHub și GitLab
# 🇬🇷 Αλλαγές λογαριασμών του GitHub και του GitLab
# 🇬🇪 GitHub და GitLab ანგარიშების გადართვები
# 🇺🇦 Перемикання облікових записів GitHub та GitLab
gh_gusbemacbe()
{
  git config user.name "Gustavo Benedetto Conti Papi"
  git config user.email "gusbemacbe@gmail.com"
  eval $(ssh-agent -s)
  ssh-add ~/.ssh/github-gusbemacbe
}

gl_gusbemacbe()
{
  git config user.name "Gustavo Benedetto Conti Papi"
  git config user.email "gusbemacbe@gmail.com"
  eval $(ssh-agent -s)
  ssh-add ~/.ssh/gitlab-gusbemacbe
}

# 🇬🇧 Syntax highlighting «cat» if output is a terminal
# 🇫🇷 Surlignant syntaxiquement le « cat » si la sortie est un terminal
# 🇪🇸 Resaltando sintacticamente el «cat» si la salida es un terminal
# 🇮🇹 Evidenziando sintatticamente il «cat» se l’output è un terminale
# 🇵🇹 Realcando sintaticamente o «cat» se a saída for um terminal
# 🇩🇪 Syntaxhervorhebung für „cat“, wenn Ausgabe ein Terminal ist
# 🇳🇱 Syntaxherhaling voor „cat” als uitvoer een terminal is
# 🏴󠁥󠁳󠁣󠁴󠁿 Realçant sintàcticament el «cat» si la sortida és un terminal
# 🇷🇴 Evidențiind sintactic „cat” dacă ieșirea este un terminal
# 🇬🇷 Χρωματική αναγνώριση σύνταξης του «cat» αν η έξοδος είναι τερματικό
# 🇬🇪 სინტაქსური ხაზგასმა „cat“-ისათვის, თუ გამოსავალი არის ტერმინალი
# 🇺🇦 Підсвічування синтаксису «cat», якщо вихід - термінал
catt() 
{
  # 🇬🇧 Checking if output is a terminal, else use regular cat
  # 🇫🇷 Vérifie si la sortie est un terminal, sinon utilise « cat »
  # 🇪🇸 Verifica si la salida es un terminal, de lo contrario, utiliza «cat»
  # 🇮🇹 Controlla se l’output è un terminale, altrimenti usa «cat»
  # 🇵🇹 Verifica se a saída é um terminal, caso contrário, utiliza «cat»
  # 🇩🇪 Überprüfe, ob die Ausgabe ein Terminal ist, andernfalls verwende „cat“
  # 🇳🇱 Controleer of de uitvoer een terminal is, anders gebruik je „cat”
  # 🏴󠁥󠁳󠁣󠁴󠁿 Comprova si la sortida és un terminal, si no, utilitza «cat»
  # 🇷🇴 Verifică dacă ieșirea este un terminal, altfel folosește „cat”
  # 🇬🇷 Έλεγξε αν η έξοδος είναι τερματικό, διαφορετικά χρησιμοποίησε το «cat»
  # 🇬🇪 შეამოწმეთ, არის თუ არა გამოსავალი ტერმინალი, წინააღმდეგ შემთხვევაში გამოიყენეთ „cat“
  # 🇺🇦 Перевірте, чи є вихід терміналом, інакше використовуйте «cat»
  if [ -t 1 ]; then
    if [ -x $(command -v nvimpager) ]; then
      nvimpager -c "$@"
    else
      /usr/bin/cat "$@"
    fi
  else
    /usr/bin/cat "$@"
  fi
}

batt()
{
  if [ -t 1 ]; then
    if [ -x $(command -v nvimpager) ]; then
      nvimpager -c "$@"
    else
      /usr/bin/bat "$@"
    fi
  else
    /usr/bin/bat "$@"
  fi
}

# 🇬🇧 The purpose of this code is to execute the ‘clear’ command before every command execution in the current shell
# 🇫🇷 Le but de ce code est d’exécuter la commande « clear » avant chaque exécution de commande dans le shell actuel
# 🇪🇸 El propósito de este código es ejecutar el comando «clear» antes de cada ejecución de comando en el shell actual
# 🇮🇹 Lo scopo di questo codice è eseguire il comando «clear» prima di ogni esecuzione di comando nella shell attuale
# 🇵🇹 O propósito deste código é executar o comando «clear» antes de cada execução de comando no shell atual
# 🇩🇪 Der Zweck dieses Codes ist es, den Befehl „clear“ vor jeder Befehlsausführung im aktuellen Terminal auszuführen
# 🇳🇱 Het doel van deze code is om het commando „clear” uit te voeren voordat elke opdracht in de huidige shell wordt uitgevoerd
# 🏴󠁥󠁳󠁣󠁴󠁿 L’objectiu d’aquest codi és executar la comanda «clear» abans de cada execució de comanda a la shell actual
# 🇷🇴 Scopul acestui cod este de a executa comanda „clear” înainte de fiecare execuție de comandă în shell-ul actual
# 🇬🇷 Ο σκοπός αυτού του κώδικα είναι να εκτελεί την εντολή «clear» πριν από κάθε εκτέλεση εντολής στο τρέχον τερματικό
# 🇬🇪 ამ კოდის მიზანია შეასრულოს ბრძანება „clear“ თითოეული ბრძანების შესრულების წინ მიმდინარე ტერმინალში
# 🇺🇦 Мета цього коду - виконати команду «clear» перед кожним виконанням команди в поточному терміналі
cb4() 
{
  preexec() 
  {
    # shellcheck disable=SC2317
    clear
  }
}

# 🇬🇧 Encrypted files in format tar with zstd compression
# 🇫🇷 Fichiers cryptés au format tar avec compression zstd
# 🇪🇸 Archivos cifrados en formato tar con compresión zstd
# 🇮🇹 File criptati in formato tar con compressione zstd
# 🇵🇹 Ficheiros criptografados em formato de tar com compactação zstd
# 🇩🇪 Verschlüsselte Dateien im tar-Format mit zstd-Kompression
# 🇳🇱 Versleutelde bestanden in tar-formaat met zstd-compressie
# 🏴󠁥󠁳󠁣󠁴󠁿 Fitxers xifrats en format tar amb compressió zstd
# 🇷🇴 Fișiere criptate în format tar cu compresie zstd
# 🇬🇷 Κρυπτογραφημένα αρχεία σε μορφή tar με συμπίεση zstd
# 🇬🇪 დაშიფრული ფაილები tar ფორმატში zstd კომპრესით
# 🇺🇦 Зашифровані файли у форматі tar з компресією zstd
cgpgtar() { tar cf - --zstd "$1" | gpg -e -z 0 > "$1.tar.zst.gpg"; }
xgpgtar() { gpg -d "$1" | tar x --zstd; }

# 🇬🇧 Colour picker
# 🇫🇷 Sélecteur de couleurs
# 🇪🇸 Selector de colores
# 🇮🇹 Selettore di colori
# 🇵🇹 Selecionador de cores
# 🇩🇪 Farbauswähler
# 🇳🇱 Kleurenkiezer
# 🏴󠁥󠁳󠁣󠁴󠁿 Selector de colors
# 🇷🇴 Selector de culori
# 🇬🇷 Επιλογέας χρωμάτων
# 🇬🇪 ფერების არჩევანი
# 🇺🇦 Вибір кольорів
if [ -n "$WAYLAND_DISPLAY" ]; then
  cpick() 
  { 
    grim -g "$(slurp -p)" -t ppm - | magick convert - -format "%[pixel:p{0,0}]" txt:- 
  }
else
  cpick() 
  { 
    xcolor --selection
  }
fi

# 🇬🇧 Monitor CPU frequence
# 🇫🇷 Fréquence du CPU du moniteur
# 🇪🇸 Frecuencia de la CPU del monitor
# 🇮🇹 Frequenza della CPU del monitor
# 🇵🇹 Frequência do CPU do monitor
# 🇩🇪 CPU-Frequenz des Monitors
# 🇳🇱 CPU-frequentie van de monitor
# 🏴󠁥󠁳󠁣󠁴󠁿 Frequència del CPU del monitor
# 🇷🇴 Frecvența CPU-ului monitorului
# 🇬🇷 Συχνότητα CPU της οθόνης
# 🇬🇪 მონიტორის CPU სიხშირე
# 🇺🇦 Частота ЦП монітора
cpufreq() { watch -n 1 eval "cat /proc/cpuinfo | grep MHz"; }

# 🇬🇧 Markdown converter with Pandoc
# 🇫🇷 Convertisseur Markdown avec Pandoc
# 🇪🇸 Convertidor de Markdown con Pandoc
# 🇮🇹 Convertitore Markdown con Pandoc
# 🇵🇹 Conversor de Markdown com Pandoc
# 🇩🇪 Markdown-Konverter mit Pandoc
# 🇳🇱 Markdown-converter met Pandoc
# 🏴󠁥󠁳󠁣󠁴󠁿 Convertidor de Markdown amb Pandoc
# 🇷🇴 Convertorul Markdown cu Pandoc
# 🇬🇷 Μετατροπέας Markdown με Pandoc
# 🇬🇪 Markdown-კონვერტერი Pandoc-თან
# 🇺🇦 Конвертер Markdown з Pandoc

## 🇬🇧 Markdown with a Piltover CSS style
## 🇫🇷 Markdown avec un style de CSS de Piltover
## 🇪🇸 Markdown con un estilo de CSS de Piltover
## 🇮🇹 Markdown con uno stile CSS di Piltover
## 🇵🇹 Markdown com um estilo de CSS de Piltover
## 🇩🇪 Markdown mit einem CSS-Stil von Piltover
## 🇳🇱 Markdown met een CSS-stijl van Piltover
## 🏴󠁥󠁳󠁣󠁴󠁿 Markdown amb un estil CSS de Piltover
## 🇷🇴 Markdown cu un stil CSS de Piltover
## 🇬🇷 Markdown με ένα στυλ CSS του Piltover
## 🇬🇪 Markdown Piltover-ის CSS სტილით
function md_piltover()
{
  pandoc -c ~/.local/share/assets/css/piltover.css -s "$1.md" -o "/tmp/$1.html"
  xdg-open "/tmp/$1.html"
}

## 🇬🇧 Markdown with a Piltover CSS style without XDG Open
## 🇫🇷 Markdown avec un style de CSS de Piltover sans XDG Open
## 🇪🇸 Markdown con un estilo de CSS de Piltover sin XDG Open
## 🇮🇹 Markdown con uno stile CSS di Piltover senza XDG Open
## 🇵🇹 Markdown com um estilo de CSS de Piltover sem XDG Open
## 🇩🇪 Markdown mit einem CSS-Stil von Piltover ohne XDG Open
## 🇳🇱 Markdown met een CSS-stijl van Piltover zonder XDG Open
## 🏴󠁥󠁳󠁣󠁴󠁿 Markdown amb un estil CSS de Piltover sense XDG Open
## 🇷🇴 Markdown cu un stil CSS de Piltover fără XDG Open
## 🇬🇷 Markdown με ένα στυλ CSS του Piltover χωρίς XDG Open
## 🇬🇪 Markdown Piltover-ის CSS სტილით XDG Open-ის გარეშე
function md_piltover_without_xdg_open()
{
  pandoc -c ~/.local/share/assets/css/piltover.css -s "$1.md" -o "$1.html"
}

# 🇬🇧 Resizing the application window
# 🇫🇷 Redimensionner la fenêtre de l’application
# 🇪🇸 Redimensionar la ventana de la aplicación
# 🇮🇹 Ridimensionare la finestra dell’applicazione
# 🇵🇹 Redimensionar a janela da aplicação
# 🇩🇪 Die Anwendungsfenstergrôbe ändern
# 🇳🇱 De toepassingsvenster vergroten
# 🏴󠁥󠁳󠁣󠁴󠁿 Redimensionar la finestra de l’aplicació
# 🇷🇴 Redimensionați fereastra aplicației
# 🇬🇷 Επεξεργασία της οθόνης της εφαρμογής
# 🇬🇪 მონიტორის ფანქრის გადართვა
# 🇺🇦 Переміщення вікна програми
# xdotool search --onlyvisible --name dolphin
# xdotool search --classname dolphin
# xdotool windowsize 54525956 1440 1100;
function dolphin_wm_0680()
{
  xdotool search --classname dolphin windowsize %@ 1440 680;
}

function dolphin_wm_1100()
{
  xdotool search --classname dolphin windowsize %@ 1440 1100;
}

function edge_wm()
{
  xdotool search --classname microsoft-edge windowsize %@ 1440 1100;
}

function firefox_wm()
{
  xdotool search --classname firefox windowsize %@ 1440 1100;
}

function code_wm()
{
  xdotool search --classname code windowsize %@ 1440 1100;
}

function gterm_wm()
{
  xdotool search --classname org.gnome.Terminal windowsize %@ 1100 760;
}